/**
 * Returns an array of 52 Cards
 * @returns {Array} deck - a deck of cards
 */
const getDeck = () => {
  const suits = ['hearts', 'spades', 'clubs', 'diamonds'];
  let deck = [];
  for (let suit of suits) {
      for (let j=1; j<=13; j++) {
          let card = {};
          switch(j) {
              case 1:
                  card.displayVal = 'Ace';
                  card.val = 11;
                  break;
              case 11:
                  card.displayVal = 'Jack';
                  card.val = 10;
                  break;
              case 12:
                  card.displayVal = 'Queen';
                  card.val = 10;
                  break;
              case 13:
                  card.displayVal = 'King';
                  card.val = 10;
                  break;
              default:
                  card.displayVal = j.toString();
                  card.val = j;
          }
          card.suit = suit;
          deck.push(card);
      }
  }
  return deck;
}



// CHECKS
const deck = getDeck();
console.log(`Deck length equals 52? ${deck.length === 52}`);

const randomCard = deck[Math.floor(Math.random() * 52)];

const cardHasVal = randomCard && randomCard.val && typeof randomCard.val === 'number';
console.log(`Random card has val? ${cardHasVal}`);

const cardHasSuit = randomCard && randomCard.suit && typeof randomCard.suit === 'string';
console.log(`Random card has suit? ${cardHasSuit}`);

const cardHasDisplayVal = randomCard &&
  randomCard.displayVal &&
  typeof randomCard.displayVal === 'string';
console.log(`Random card has display value? ${cardHasDisplayVal}`);