const blackjackDeck = getDeck();

/**
 * Represents a card player (including dealer).
 * @constructor
 * @param {string} name - The name of the player
 */
class CardPlayer {
  constructor(name) {
    this.name = name;
    this.hand = [];
  }
  drawCard() {
    const randomCard = deck[Math.floor(Math.random() * 52)];
    this.hand.push(randomCard);
  }
};

// CREATE TWO NEW CardPlayers
const dealer = new CardPlayer('Dealer');
const player = new CardPlayer('Player');

/**
 * Calculates the score of a Blackjack hand
 * @param {Array} hand - Array of card objects with val, displayVal, suit properties
 * @returns {Object} blackJackScore
 * @returns {number} blackJackScore.total
 * @returns {boolean} blackJackScore.isSoft
 */
const calcPoints = (hand) => {
  let points = {isSoft: false, total: 0};
  hand.forEach(card => {
    if (card.displayVal == 'Ace'){
      if ((points.total + card.val) > 21) {
        card.val = 1;
      } else {
        points.isSoft = true;
      }
    }
    points.total += card.val;
  });
  return points;
}

/**
 * Determines whether the dealer should draw another card.
 * 
 * @param {Array} dealerHand Array of card objects with val, displayVal, suit properties
 * @returns {boolean} whether dealer should draw another card
 */
const dealerShouldDraw = (dealerHand) => {
  let points = calcPoints(dealerHand);
  if (points.total <= 16) {
    return true;
  } else if (points.total == 17 && points.isSoft == true) {
    return true;
  } else {
    return false;
  }
}

/**
 * Determines the winner if both player and dealer stand
 * @param {number} playerScore 
 * @param {number} dealerScore 
 * @returns {string} Shows the player's score, the dealer's score, and who wins
 */
const determineWinner = (playerScore, dealerScore) => {
  let result = `Player's score: ${playerScore}\nDealer's score: ${dealerScore}\n`;
  if (playerScore > dealerScore) {
    result += `You win!`;
  } else if (playerScore == dealerScore) {
    result += `It's a tie!`;
  } else {
    result += `Dealer wins!`;
  }
  return result;
}

/**
 * Creates user prompt to ask if they'd like to draw a card
 * @param {number} count 
 * @param {string} dealerCard 
 */
const getMessage = (count, dealerCard) => {
  return `Dealer showing ${dealerCard.displayVal}, your count is ${count}.  Draw card?`
}

/**
 * Logs the player's hand to the console
 * @param {CardPlayer} player 
 */
const showHand = (player) => {
  const displayHand = player.hand.map((card) => card.displayVal);
  alert(`${player.name}'s hand is ${displayHand.join(', ')} (${calcPoints(player.hand).total})`);
}

/**
 * Runs Blackjack Game
 */
const startGame = function() {
  player.drawCard();
  dealer.drawCard();
  player.drawCard();
  dealer.drawCard();

  let playerScore = calcPoints(player.hand).total;
  if (playerScore == 21) {
    return 'Blackjack - you win!';
  }

  showHand(player);
  while (playerScore < 21 && confirm(getMessage(playerScore, dealer.hand[0]))) {
    player.drawCard();
    playerScore = calcPoints(player.hand).total;
    showHand(player);
  }
  if (playerScore > 21) {
    return 'You went over 21 - you lose!';
  }
  alert(`Player stands at ${playerScore}`);

  let dealerScore = calcPoints(dealer.hand).total;
  if (dealerScore == 21) {
    return 'Dealer\'s blackjack - you lose!';
  }
  while (dealerScore < 21 && dealerShouldDraw(dealer.hand)) {
    dealer.drawCard();
    dealerScore = calcPoints(dealer.hand).total;
    showHand(dealer);
  }
  if (dealerScore > 21) {
    return 'Dealer went over 21 - you win!';
  }
  alert(`Dealer stands at ${dealerScore}`);

  return determineWinner(playerScore, dealerScore);
}
alert(startGame());